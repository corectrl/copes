Please, choose a template for your issue using the **template selector** under the *Description* field.

Any issues not following the [Issues writing guidelines](CONTRIBUTING.md#Issues-writing-guidelines) **may be closed as invalid** without further explanation.
