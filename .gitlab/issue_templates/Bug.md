Please, follow the [Issues writing guidelines](CONTRIBUTING.md#Issues-writing-guidelines).

**pecmon version:**

**Distribution:**

**Kernel version:**

Please, add more info if needed, like desktop enviroment or other system components.

# Description of the bug

Please, describe your bug here. Be precise and clear.

# Reproduction

Simplified, easy to follow steps to reproduce the bug:

1.
2.
3.

**Actual result:**

**Expected result:**

# Additional information

Include other files here (screenshots, logs, backtraces...). For logs, backtraces or console output, use text format instead of screenshots.
